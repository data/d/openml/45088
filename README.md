# OpenML dataset: DLBCL

https://www.openml.org/d/45088

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

**Diffuse large B-cell lymphoma (DLBCL) dataset**

**Authors**: M. Shipp, K. Ross, P. Tamayo, A. Weng, J. Kutok, R. Aguiar, M. Gaasenbeek, M. Angelo, M. Reich, G. Pinkus, et al

**Please cite**: ([URL](https://www.scopus.com/record/display.uri?eid=2-s2.0-18244409933&origin=inward)): M. Shipp, K. Ross, P. Tamayo, A. Weng, J. Kutok, R. Aguiar, M. Gaasenbeek, M. Angelo, M. Reich, G. Pinkus, et al, Diffuse large b-cell lymphoma outcome prediction by gene-expression profiling and supervised machine learning, Nat. Med. 8 (1) (2002) 68-74

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/45088) of an [OpenML dataset](https://www.openml.org/d/45088). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/45088/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/45088/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/45088/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

